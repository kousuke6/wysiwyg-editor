/**
 * テキストエリアのサイズを中身に合わせて伸縮
 * @param string textarea
 */
function textAreaAdjust(textarea) {
    $(textarea).on("input", function (evt) {
        if (evt.target.scrollHeight > evt.target.offsetHeight) {
            $(evt.target).height(evt.target.scrollHeight);
        } else {
            var lineHeight = Number($(evt.target).css("lineHeight").split("px")[0]);
            while (true) {
                $(evt.target).height($(evt.target).height() - lineHeight);
                if (evt.target.scrollHeight > evt.target.offsetHeight) {
                    $(evt.target).height(evt.target.scrollHeight);
                    break;
                }
            }
        }
    });
}

//iOS用 戻るボタン
$('#movetoBackWrap').on('click', function () {
    history.back();
})

/**
 * アップロードした画像その場で表示
 * @param string input
 * @param string previewBox
 */
function imgPreview(input, previewBox) {
    // if ($(input).files.length) {
    //     return;
    // }
    var file = $(input).prop('files')[0];
    var fr = new FileReader();
    $(previewBox).css('background-image', 'none');
    fr.onload = function () {
        $(previewBox).css('background-image', 'url(' + fr.result + ')');
    }
    fr.readAsDataURL(file);
    $(previewBox + ' img').css('opacity', 0);
}

/**
 * モーダルの表示
 * @param string modalType
 * @param string modalBody
 * @param boolean modalOutFlag
 */
var modal = function (modalType, modalBody, modalOutFlag = true) {
    $("html div:first").append('<div id="modalBg"></div>')
    $("html div:first").append('<div id="modal"></div>');
    $('#modal').append('<div id="modalBody"></div>');
    $('#modalBody').html(modalBody);
    if (modalType == 'error') {
        $('#modal').prepend('<div id="errorModalTitle">エラー</div>');
    }

    if (modalOutFlag == true) {
        //画面のどこかをクリックしたらモーダルを閉じる
        $("#modal,#modalBg").click(function () {
            modalOut();
        });
    }

    //画面中央を計算する関数を実行
    modalResize();
    //モーダルウィンドウを表示
    $("#modalBg,#modal").fadeIn("fast");
    //画面の大きさ変更にも対応
    $(window).resize(modalResize);

    //画面の左上からmodal-mainの横幅・高さを引き、その値を2で割ると画面中央の位置が計算できます
    function modalResize() {
        var w = $(window).width();
        var h = $(window).height();
        var cw = $("#modal").outerWidth();
        var ch = $("#modal").outerHeight();
        //取得した値をcssに追加する
        $("#modal").css({
            "left": ((w - cw) / 2) + "px",
            "top": ((h - ch) / 2) + "px"
        });
    }
};

/**
 * モーダルの削除
 */
var modalOut = function () {
    $("#modal,#modalBg").fadeOut("fast", function () {
        //挿入した<div id="modal-bg"></div>を削除
        $('#modal,#modalBg').remove();
    });
}

/**
 * ローディングインジケータの表示
 */
var showLoading = function () {
    $('body').append('<div class="loadingWrap" style="display:none;"><div class="sk-fading-circle"><div class="sk-circle1 sk-circle"></div><div class="sk-circle2 sk-circle"></div><div class="sk-circle3 sk-circle"></div><div class="sk-circle4 sk-circle"></div><div class="sk-circle5 sk-circle"></div><div class="sk-circle6 sk-circle"></div><div class="sk-circle7 sk-circle"></div><div class="sk-circle8 sk-circle"></div><div class="sk-circle9 sk-circle"></div><div class="sk-circle10 sk-circle"></div><div class="sk-circle11 sk-circle"></div><div class="sk-circle12 sk-circle"></div></div></div>')
    $('.loadingWrap').show();
}

/**
 * ローディングインジケータの非表示
 */
var removeLoading = function () {
    $('.loadingWrap').hide();
    $('.loadingWrap').remove();
}

/**
 * 各月の最後の日を取得
 * @param int year
 * @param int month
 */
function getLastDate(year, month) {
    var lastday = new Array('', 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31);
    if ((year % 4 == 0 && year % 100 != 0) || year % 400 == 0) {
        lastday[2] = 29;
    }
    return lastday[month];
}

/**
 * セレクトボックスに選択肢を追加
 *
 * @param array arg
 */
function addOption(arg) {
    var i, option;
    option = null;
    dayList = ["日", "月", "火", "水", "木", "金", "土"]
    if (arg.for == 'userReserveDate') {
        var today = new Date();
        var thisYear = today.getFullYear();
        var thisMonth = today.getMonth() + 1;
        var thisDate = today.getDate();
        var date = thisDate;
        var year = thisYear;
        var lastDate = getLastDate(thisYear, thisMonth);
        var plusDate = 0;
        var dayNum = today.getDay();
        for (i = 0; i < 30; i++) {
            //指定された曜日の時だけ追加
            if ($.inArray(String(dayNum), arg.availableDay) != -1) {
                option += '<option value="' + year + '-' + addZero(2,thisMonth) + '-' + addZero(2,date + plusDate) + '">' + thisMonth + '/' + (date + plusDate) + '(' + dayList[dayNum] + ')' + '</option>';
            }
            if (dayNum == 6) {
                dayNum = 0;
            } else {
                dayNum++;
            }
            if ((date + plusDate) == lastDate) {
                if (thisMonth == 12) {
                    thisMonth = 1;
                    year++;
                } else {
                    thisMonth++;
                }
                lastDate = getLastDate(year, thisMonth);
                date = 1;
                plusDate = 0;
            } else{
            plusDate++;
            }
        }
    } else if (arg.for == 'userReserveTime') {
        var times = arg.availableTime.split(/[-:]/)
        var minute = Number(times[1]);
        var timeInterval = 30;
        var plusHour = 0;
        var trigger = true;
        var i = 1;
        while (true) {
            if (minute >= 60 * i) {
                plusHour++;
                i++;
                if (i == 5) {
                    trigger = false;
                }
            }
            option += '<option value="' + addZero(2, (Number(times[0]) + plusHour) % 24) + ':' + addZero(2, minute % 60) + ':00">' + addZero(2, (Number(times[0]) + plusHour) % 24) + ':' + addZero(2, minute % 60)  + '</option >';
            if ((Number(times[0]) + plusHour) % 24 == Number(times[2]) && (minute % 60 + timeInterval) > Number(times[3])) {
                break;
            }
            minute += timeInterval;
        }
    } else {
        for (i = arg.start; i <= arg.end; i++) {
            if (isset(arg.thisDay) && i === arg.thisDay) {
                option += "<option value='" + i + "' selected>" + i + "</option>";
            } else {
                option += "<option value='" + i + "'>" + i + "</option>";
            }
        }
    }
    $(arg.select_box).html(option);
};



