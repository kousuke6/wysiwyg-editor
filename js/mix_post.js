// テスト用
// $(document).on("click", "#testDataBtn", function () {
//   let shop_id = $('.submit').attr('data-shopId');
//   let editableBody = $('#editableBody').html();
//   let dateObj = new Date();
//   //画像のプレフィックス用日付
//   let nowTime = dateObj.getFullYear() + addZero(dateObj.getMonth() + 1) + addZero(dateObj.getDate())
//   + addZero(dateObj.getHours()) + addZero(dateObj.getMinutes());
//   // console.log(editableBody);//fixed
//   let body_and_alts = editToDBverText(editableBody, nowTime, shop_id)

//   console.log(body_and_alts);//fixed
  
//   // console.log(body_and_alts['body']);//fixed
//   // $('#dataToDb').val();
// });






var caret;// キャレット

// (function ($) {
//   $.fn.wysiwygEvt = function () {
//     return this.each(function () {
//       var $this = $(this);
//       var htmlOld = $this.html();
//       $this.on('blur keyup paste copy cut mouseup', function () {
//         var htmlNew = $this.html();
//         if (htmlOld !== htmlNew) {
//           $this.trigger('change');
//           htmlOld = htmlNew;
//         }
//       });
//     });
//   };
// })(jQuery);


$(function () {
  //    $('#editableBody').wysiwygEvt();

});


// 送信ボタン押下時処理
var mixPost = function (post_for) {
  // showLoading();//回転ローディング標示

  //console.log($('#editableBody').html());
  // let post_for = $(this).attr('data-posttype');
  let shop_id = $('.submit').attr('data-shopId');
  let editableBody = $('#editableBody').html();
  let dateObj = new Date();

  //画像のプレフィックス用日付
  let nowTime = dateObj.getFullYear() + addZero(dateObj.getMonth() + 1) + addZero(dateObj.getDate())
    + addZero(dateObj.getHours()) + addZero(dateObj.getMinutes());

  let body_and_alts = editToDBverText(editableBody, nowTime, shop_id)



  if (post_for == 'blog') {
    // var kijiTitle = $('#editableTitle').val();
    // $('input[name=blog_title]').val(kijiTitle);
    $('input[name=blog_body]').val(body_and_alts['body']);
  }
  else if (post_for == 'menu') {
    // var menuTitle = $('#editableTitle').val();
    // $('input[name=menu_title]').val(menuTitle);
    $('input[name=menu_body]').val(body_and_alts['body']);
  }

  if (body_and_alts['imgs'] != '') {
    let input_alts = []
    let input_base64s = []
    for (var i in body_and_alts['imgs']) {
      input_alts.push(body_and_alts['imgs'][i]['alt']);
      input_base64s.push(body_and_alts['imgs'][i]['base64']);
    }
    $('input[name=img_names]').val(input_alts);
    $('input[name=img_base64s]').val(input_base64s);//もし新規画像があればhiddenにいれる。（編集の時は、初めからある画像ははじく）
  }
  // $('#form').submit();

  $('textarea[name=dataToDb]').val(body_and_alts['body']);
}

let editToDBverText = function (editableBodyA, nowTimeA, shop_id) {
  let array = [];
  let base64s = [];
  let tagged_alts = [];
  array['body'] = editableBodyA;
  array['imgs'] = [];
  let imgs = editableBodyA.match(/<img(.|\s)*?>/gi);
  let alts = editableBodyA.match(/alt="[&＆0-9A-Za-z０-９ａ-ｚＡ-Ｚ!#%,+-‘;=@\^\'\._\-\/\{\}\s\(\)\[\]\+\$！”＃＄％＆ぁ-んァ-ン一-龥０-９ａ-ｚＡ-Ｚ’（）＝～｜‘｛＋＊｝＜＞？＿－ー＾￥＠「；：」、。・ｧ-ﾝヴ]+"/gi);//altsは構造上、グローバルにしている
  let srcs = editableBodyA.match(/src="[0-9A-Za-z!#%,+-‘;=@\^\'\._\-\/\{\}\s\(\)\[\]\+\$]+"/gi);

  if (imgs) {
    var count = imgs.length;

    for (var i = 0; i < count; i++) {
      alts[i] = alts[i].substr(0, alts[i].length - 1);
      alts[i] = alts[i].substr(5);
      srcs[i] = srcs[i].substr(0, srcs[i].length - 1);
      srcs[i] = srcs[i].substr(5);

      if ((alts[i].startsWith(shop_id) == false) && (srcs[i].startsWith('data:') == true)) {
        alts[i] = alts[i].replace(/[%+$&＆,‘\']+/gi, '');
        alts[i] = shop_id + '_' + nowTimeA + '_' + alts[i];
        base64s[i] = btoa(srcs[i]);
        base64s[i] = base64s[i].replace(/^.*,/, '');
        array['imgs'][i] = { base64: base64s[i], alt: alts[i] };
      }

      tagged_alts[i] = '[.+.]' + alts[i] + '[.+.]';
      array['body'] = array['body'].replace(imgs[i], tagged_alts[i])
    }
  }

  return array;
};

var toBase64s = function (txt) {//画像のsrcからbase64データを作って、それをフォームに入れる

  var srcs = txt.match(/src="[0-9A-Za-z!#%,+-‘;=@\^\'\._\-\/\{\}\s\(\)\[\]\+\$]+"/gi);
  var base64s = [];

  if (srcs) {
    for (var i = 0; i < srcs.length; i++) {
      srcs[i] = srcs[i].substr(0, srcs[i].length - 1);
      srcs[i] = srcs[i].substr(5);
      base64s[i] = btoa(srcs[i]);
      base64s[i] = base64s[i].replace(/^.*,/, '');
    }//for
  }//if
  return base64s;
};

// 文字列挿入ボタン押下時処理
$('.insStr').on('click', function () {

  showLoading();//回転ローディング標示

  // 文字列を挿入
  insertHTML($('#editableBody'), '"テスト文字列"');
});

var uploaded = 0;

// 画像をアップロードするボタン
$(document).on('change', 'input[type="file"]', function (e) {
  showLoading();//回転ローディング標示

  var target = this;

  if (e.target.files.length > 5 - uploaded) {
    alert('選択ファイルが多すぎます');
    return false;
  }

  // 画像リサイズ処理
  resize(
    e,
    setImages,
    // startProc
    function () {
      $('.file').addClass('loading disabled');
      $(target).prop('disabled', true);
    },
    // endProc
    function (cnt) {
      $('.file').removeClass('loading');
      uploaded += cnt;
      if (uploaded < 5) {
        $('.file').removeClass('disabled');
        $(target).prop('disabled', false);
      }
    }
  );
});

var addZero = function (n) {
  return n < 10 ? '0' + n : '' + n;
}

var uploadImgData = function (alts, srcs, k) {
  var base64 = btoa(srcs[k]);
  base64 = base64.replace(/^.*,/, '');


  //var file_name = encodeURIComponent(alts[i]);
  var file_name = alts[k];

  $.ajax({
    type: "POST",
    url: "kiji_proto_php1.php",
    data: 'base64=' + base64 + '&file_name=' + file_name,
    cache: false,
    dataType: "text",

    success: function (res, type) {
      $('#res').html(res);
    },

    error: function (xmlHttpRequest, textStatus, errorThrown) {
      console.log(xmlHttpRequest.responseText);
      alert(textStatus);
    }

  });// ajax
};

// #editable変更時処理
$('#editableBody').on('blur keyup paste copy cut mouseup', function () {
  caret = getCaretPosition(this);
  $('#caret').html(caret);

  uploaded = $(this).find('.imgPrev').length;
  $('#uploaded').html(uploaded);

  if (uploaded < 5) {
    $('.file').removeClass('disabled');
    $('input[type="file"]').prop('disabled', false);
  } else {
    $('.file').addClass('disabled');
    $('input[type="file"]').prop('disabled', true);
  }
});

// キャレット位置取得
var getCaretPosition = function (editableDiv) {
  var caretPos = 0,
    sel, range;
  if (window.getSelection) {
    sel = window.getSelection();
    if (sel.rangeCount) {
      range = sel.getRangeAt(0);
      if (range.commonAncestorContainer.parentNode == editableDiv) {
        caretPos = range.endOffset;
      }
    }
  } else if (document.selection && document.selection.createRange) {
    range = document.selection.createRange();
    if (range.parentElement() == editableDiv) {
      var tempEl = document.createElement("span");
      editableDiv.insertBefore(tempEl, editableDiv.firstChild);
      var tempRange = range.duplicate();
      tempRange.moveToElementText(tempEl);
      tempRange.setEndPoint("EndToEnd", range);
      caretPos = tempRange.text.length;
    }
  }
  return caretPos;
};

// contentEditable要素へのHTML挿入処理
var insertHTML = function (elm, html) {

  // これがないと挿入できない
  $(elm).focus();

  if (document.selection) {
    // IE11未満
    var selection = document.selection();
    var range = selection.createRange();
    range.pasteHTML(html);
  } else if (navigator.userAgent.toLowerCase().indexOf('trident') !== -1) {
    // IE11
    //        document.execCommand('paste', false, html);
    document.execCommand('ms-beginUndoUnit');

    var range = document.createRange();
    var sel = window.getSelection();
    range.setStart(elm, 5);
    range.collapse(true);
    sel.removeAllRanges();
    sel.addRange(range);

    //        var r = document.getSelection().getRangeAt(0);
    var r = sel.getRangeAt(0);
    r.deleteContents();
    r.insertNode(r.createContextualFragment(html));
    document.execCommand('ms-endUndoUnit');
  } else {
    // Chrome, Firefox, Safari, Opera etc
    document.execCommand('insertHTML', false, html);
  }
};


// 選択画像セット
var setImages = function (f, canvas) {

  var div = $('<div></div>');

  //var displaySrc = canvas.toDataURL('image/jpeg', 0.9);
  var displaySrc = canvas.toDataURL(f.type);
  var displayImg = $('<img>');
  displayImg.attr('class', 'imgPrev');
  displayImg.attr('src', displaySrc);
  displayImg.attr('alt', f.name);
  // displayImg.attr('style', 'max-width:100px; max-height:100px;');
  div.append(displayImg);
  /*
  var displayA = document.createElement('a');
  displayA.href = displaySrc;
  displayA.setAttribute('data-size', canvas.width + 'x' + canvas.height);
  displayA.appendChild(displayImg);
  $('.my-gallery div:last-child').append(displayA);
  
  var inputText = document.createElement('input');
  inputText.type = 'text';
  inputText.name = 'galt';
  inputText.value = f.name;
  inputText.setAttribute('placeholder', '写真の説明を任意で入力してください（20文字以内）');
  $('.my-gallery div:last-child').append(inputText);
  
  var btn = document.createElement('button');
  btn.innerHTML = '削除';
  btn.type = 'button';
  btn.className = 'delImg';
  $('.my-gallery div:last-child').append(btn);
  */
  // imgタグを挿入
  insertHTML($('#editableBody'), div.html());
  // $('.loading_wrap').hide();//回転ローディング消す
  removeLoading();//回転ローディング消す


};



