
$(document).on('click', function (e) {
    if ($(e.target).closest('#hamburger_wrap').length) {
        if ($('#hamburger_wrap').hasClass('working')) {
            $('#drawer_menu').css('visibility', 'hidden');
            $('#hamburger_wrap').removeClass("working");
        } else {
            $('#drawer_menu').css('visibility', 'visible');
            $('#hamburger_wrap').addClass('working');
        }
    }
    else if (!$(e.target).closest('#drawer_menu').length && !$(e.target).closest('#hamburger_wrap').length) {
        if ($('#hamburger_wrap').hasClass('working')) {
            $('#drawer_menu').css('visibility', 'hidden');
            $('#hamburger_wrap').removeClass("working");
        }
    }
});

$(function(){
    let nav_next_tab;
    $('#footer_nav_wrap>div').on('click', function () {
      nav_next_tab = $(this);
      $('#footer_nav_wrap>div').removeClass('current_tab')
      nav_next_tab.addClass('current_tab')
    });

});

